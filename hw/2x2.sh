#!/bin/bash -x
FILE=kltab-F_Cu.ps

# Center point

CX=11.635cm
CY=15.765cm

# Half the gap between boards
#
# On a 5x5 cm panel, this yields:
#
# - 1.5 mm border,
# - 22 mm PCB (KLtab is square), 
# - 3 mm border,
# - 22 mm PCB,
# - 1.5 mm border.
#

HX=0.14cm
HY=0.14cm


rot()
{
	pstops "(-$CX,-$CY)" $FILE |
	    pstops "$1($CX,$CY)" |
	    pstops "($2,$3)" |
	    grep -v ^showpage
}


# can't use psmerge because it tries to use "pswrite" while gs now calls the
# driver "ps2write". Solution found on
# https://groups.google.com/forum/#!topic/comp.text.tex/cuSolsMaPLg

#psmerge <(rot "" $HX $HY) <(rot L 0cm 0cm) <(rot U 0cm 0cm) <(rot R 0cm 0cm)

gs -q -dNOPAUSE -dBATCH -sDEVICE=ps2write -sOutputFile=- -c save pop -f \
    <(rot "" -$HX -$HY) <(rot L $HX -$HY) <(rot U $HX $HY) <(rot R -$HX $HY) \
    <(echo showpage)
