update=Sun Jun  1 12:02:52 2014
last_client=pcbnew
[cvpcb]
version=1
NetIExt=net
[cvpcb/libraries]
EquName1=devcms
[eeschema]
version=1
LibDir=
NetFmtName=PcbnewAdvanced
RptD_X=0
RptD_Y=100
RptLab=1
LabSize=60
[eeschema/libraries]
LibName1=../../kicad-libs/components/pwr
LibName2=../../kicad-libs/components/r
LibName3=../../kicad-libs/components/c
LibName4=../../kicad-libs/components/led
LibName5=../../kicad-libs/components/powered
LibName6=../../kicad-libs/components/kl25-32
LibName7=../../kicad-libs/components/micro_usb_b
LibName8=../../kicad-libs/components/gencon
LibName9=../../kicad-libs/components/testpoint
[pcbnew]
version=1
LastNetListRead=
UseCmpFile=1
PadDrill="    0.600000"
PadDrillOvalY="    0.600000"
PadSizeH="    1.500000"
PadSizeV="    1.500000"
PcbTextSizeV="    1.500000"
PcbTextSizeH="    1.500000"
PcbTextThickness="    0.300000"
ModuleTextSizeV="    1.000000"
ModuleTextSizeH="    1.000000"
ModuleTextSizeThickness="    0.150000"
SolderMaskClearance="    0.127000"
SolderMaskMinWidth="    0.000000"
DrawSegmentWidth="    0.200000"
BoardOutlineThickness="    0.100000"
ModuleOutlineThickness="    0.150000"
[pcbnew/libraries]
LibDir=
LibName1=../../kicad-libs/modules/stdpass
LibName2=../../kicad-libs/modules/pads
LibName3=../../kicad-libs/modules/qfn
LibName4=../../kicad-libs/modules/header
LibName5=../../kicad-libs/modules/zx62-b-5pa
LibName6=../../kicad-libs/modules/pads-array
