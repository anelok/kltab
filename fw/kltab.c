#include <stdbool.h>
#include <stdint.h>

#include "gpio.h"


#define	nLED1	GPIO_ID(B, 0)
#define	nLED2	GPIO_ID(B, 1)
#define	LED3	GPIO_ID(D, 6)
#define	LED4	GPIO_ID(D, 7)


static void delay(void)
{
	uint32_t n = 1000000;

	while (n--)
		asm("");
}


static void led(uint8_t n, bool on)
{
	switch (n) {
	case 0:
		(on ? gpio_clr : gpio_set)(nLED1);
		break;
	case 1:
		(on ? gpio_clr : gpio_set)(nLED2);
		break;
	case 2:
		(on ?  gpio_set : gpio_clr)(LED3);
		break;
	case 3:
		(on ?  gpio_set : gpio_clr)(LED4);
		break;
	}
}


int main(void)
{
	uint8_t n = 0;

	gpio_init_out(nLED1, 1);
	gpio_init_out(nLED2, 1);
	gpio_init_out(LED3, 0);
	gpio_init_out(LED4, 0);

	while (1) {
		led(n & 3, 1);
		delay();
		led(n & 3, 0);
		n++;
	}
}

