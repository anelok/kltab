/*
 * fw/boot.c - DFU boot loader firmware for KLtab design
 *
 * Written 2013-2014 by Werner Almesberger
 * Copyright 2013-2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "usb-board.h"
#include "flash.h"
#include "usb.h"
#include "dfu.h"
#include "dfu-wait.h"

#include "regs.h"
#include "misc.h"
#include "board.h"
#include "gpio.h"
#include "led.h"


static void ports_off(void)
{
	/* system pins */

	gpio_init_off(GPIO_ID(A, 0));	/* SWD_CLK */
	gpio_init_off(GPIO_ID(A, 3));	/* SWD_DIO */

	/* external connectors (includes LEDs) */

	gpio_init_off(GPIO_ID(E, 30));
	gpio_init_off(GPIO_ID(A, 1));
	gpio_init_off(GPIO_ID(A, 2));
	gpio_init_off(GPIO_ID(A, 4));	/* NMI_b */

	gpio_init_off(GPIO_ID(B, 0));
	gpio_init_off(GPIO_ID(B, 1));
	gpio_init_off(GPIO_ID(C, 1));
	gpio_init_off(GPIO_ID(C, 2));
	gpio_init_off(GPIO_ID(C, 3));

	gpio_init_off(GPIO_ID(C, 4));
	gpio_init_off(GPIO_ID(C, 5));
	gpio_init_off(GPIO_ID(C, 6));
	gpio_init_off(GPIO_ID(C, 7));
	gpio_init_off(GPIO_ID(D, 4));
	gpio_init_off(GPIO_ID(D, 5));
	gpio_init_off(GPIO_ID(D, 6));
	gpio_init_off(GPIO_ID(D, 7));

	/* unconnected pins */

	gpio_init_off(GPIO_ID(A, 18));
	gpio_init_off(GPIO_ID(A, 19));
}


static void (**app_vec)(void) = (void (**)(void)) (APP_BASE | 4);


int main(void)
{
	/*
	 * @@@ we seem to reset a few times before the system stabilized.
	 * find out why.
	 */
	
	ports_off();
	led_init();

	usb_init();
	usb_begin_device();
	dfu_init();
	flash_init(); /* must follow dfu_init */
	while (dfu_wait());
	usb_end_device();

	(*app_vec)();

	reset_cpu();
}
