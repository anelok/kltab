/*
 * fw/ep0.c - EP0 extension protocol
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>

#include "usb.h"
#include "dfu.h"
#include "fmt.h"

#include "board.h"
#include "misc.h"
#include "target.h"
#include "ep0.h"


#define	MAX_RES	250


static char res_buf[MAX_RES+1];
static uint8_t res_len = 0;


static void add_char(void *user, char ch)
{
	if (res_len < MAX_RES)
		res_buf[++res_len] = ch;
}


void report(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vformat(add_char, NULL, fmt, ap);
	va_end(ap);
}


static uint16_t data_bytes;
static uint32_t data_buf[32];
static uint32_t data_offset;


static void do_write(void *user)
{
	*res_buf = !tgt_write(data_buf, data_offset, data_bytes >> 2);
}


static bool my_setup(const struct setup_request *setup)
{
	uint16_t req = setup->bmRequestType | setup->bRequest << 8;
	
	switch (req) {
	case PGM_ATTACH:
		res_len = 0;
		*res_buf = !tgt_attach();
		return 1;
	case PGM_DETACH:
		res_len = 0;
		*res_buf = !tgt_detach();
		return 1;
	case PGM_ACQUIRE:
		res_len = 0;
		*res_buf = !tgt_acquire();
		return 1;
	case PGM_IDENTIFY:
		res_len = 0;
		*res_buf = !tgt_identify();
		return 1;
	case PGM_QUERY:
		usb_send(&eps[0], res_buf, res_len+1, NULL, NULL);
		return 1;
	case PGM_ERASE:
		res_len = 0;
		*res_buf = !tgt_erase();
		return 1;
	case PGM_WRITE:
		data_bytes = setup->wLength;
		if (!data_bytes)
			return 0;
		if (data_bytes > sizeof(data_buf))
			return 0;
		if (data_bytes & 3)
			return 0;
		if (setup->wValue & 3)
			return 0;
		data_offset = (uint32_t) setup->wIndex << 16 | setup->wValue;
		res_len = 0;
		usb_recv(&eps[0], (uint8_t *) data_buf, data_bytes,
		    do_write, NULL);
		return 1;
	case PGM_READ:
		if (setup->wLength > sizeof(data_buf))
			return 0;
		if (setup->wLength & 3)
			return 0;
		if (setup->wValue & 3)
			return 0;
		res_len = 0;
		*res_buf = !tgt_read(data_buf,
		    (uint32_t) setup->wIndex << 16 | setup->wValue,
		    setup->wLength >> 2);
		if (*res_buf)
			usb_send(&eps[0], NULL, 0, NULL, NULL);
		else
			usb_send(&eps[0], data_buf, setup->wLength, NULL, NULL);
		return 1;
	default:
		return 0;
	}
}


static bool my_dfu_setup(const struct setup_request *setup)
{
	switch (setup->bmRequestType | setup->bRequest << 8) {
	case DFU_TO_DEV(DFU_DETACH):
		/* @@@ should use wTimeout */
		dfu.state = appDETACH;
		return 1;
	default:
		return dfu_setup_common(setup);
	}
}


static void my_set_interface(int nth)
{
	if (nth) {
		user_setup = my_dfu_setup;
		user_get_descriptor = dfu_my_descr;
		dfu.state = appIDLE;
	} else {
		user_setup = my_setup;
		user_get_descriptor = NULL;
	}
}

static void my_reset(void)
{
	if (dfu.state == appDETACH)
		reset_cpu();
}


void ep0_init(void)
{
	user_setup = my_setup;
	user_set_interface = my_set_interface;
	my_set_interface(0);
	user_reset = my_reset;
}
