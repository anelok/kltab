/*
 * fw/board.h - Board-specific definitions for KLtab 
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef BOARD_H
#define	BOARD_H

#include "gpio.h"

/* ----- LEDs -------------------------------------------------------------- */


#define LED_A	GPIO_ID(D, 6)
#define LED_B	GPIO_ID(D, 7)
#define nLED_C	GPIO_ID(B, 0)
#define nLED_D	GPIO_ID(B, 1)

#define	LED	LED_B

/* ----- Target SWD -------------------------------------------------------- */

#define	TGT_nRESET	GPIO_ID(C, 4)
#define	TGT_SWD_DIO	GPIO_ID(C, 5)
#define	TGT_SWD_CLK	GPIO_ID(C, 6)

/* ----- USB --------------------------------------------------------------- */

#define	DFU_USB_VENDOR	USB_VENDOR
#define	DFU_USB_PRODUCT	USB_PRODUCT

#ifndef	BOARD_MAX_mA
#define	BOARD_MAX_mA	120	/* limited by KL26's USB VREG */
#endif

#ifndef EP0_SIZE
#define	EP0_SIZE	64	/* Full-Speed */
#endif

#ifndef NUM_EPS
#define	NUM_EPS		2
#endif

/*
 * Define DFU interface names. They're only used if DFU_ALT_SETTINGS is
 * defined.
 */

#if 0
#define	DFU_ALT_NAME_0	'K', 0, 'L', 0, '2', 0, '6', 0
#define	DFU_ALT_NAME_0_IDX 1
#define	DFU_ALT_NAME_0_LEN 8
#endif

/* ----- Panic ------------------------------------------------------------- */


void panic(void) __attribute__((noreturn));

#endif /* !BOARD_H */
