/*
 * fw/pgm.c - Programmer application for KLtab
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include "usb.h"
#include "usb-board.h"

#include "sleep.h"
#include "target.h"
#include "ep0.h"


int main(void)
{
	msleep(1);	/* let host notice we've detached */

	tgt_init();

	usb_init();
	usb_begin_device();
	ep0_init();

	while (1)
		usb_poll_device();

#if 0
#include "led.h"
	led_init();
	while (1) {
		led(1);
		msleep(100);
		led(0);
		msleep(100);
	}
#endif
}
