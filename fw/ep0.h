/*
 * fw/ep0.h - KLtab programmer protocol
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef EP0_H
#define	EP0_H

#ifndef	USB_TYPE_VENDOR
#define	USB_TYPE_VENDOR	0x40
#endif

#ifndef	USB_DIR_IN
#define	USB_DIR_IN	0x80
#endif

#ifndef	USB_DIR_OUT
#define	USB_DIR_OUT	0x00
#endif


#define	PGM_ATTACH	(USB_TYPE_VENDOR | USB_DIR_OUT | 0 << 8)
#define	PGM_DETACH	(USB_TYPE_VENDOR | USB_DIR_OUT | 1 << 8)

#define	PGM_ACQUIRE	(USB_TYPE_VENDOR | USB_DIR_OUT | 0x10 << 8)
#define	PGM_IDENTIFY	(USB_TYPE_VENDOR | USB_DIR_OUT | 0x11 << 8)
#define	PGM_QUERY	(USB_TYPE_VENDOR | USB_DIR_IN | 0x12 << 8)

#define	PGM_ERASE	(USB_TYPE_VENDOR | USB_DIR_OUT | 0x20 << 8)
#define	PGM_WRITE	(USB_TYPE_VENDOR | USB_DIR_OUT | 0x21 << 8)
#define	PGM_READ	(USB_TYPE_VENDOR | USB_DIR_IN | 0x22 << 8)


void report(const char *fmt, ...);

void ep0_init(void);

#endif /* !EP0_H */
