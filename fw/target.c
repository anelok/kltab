/*
 * fw/target.c - SWD target control
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "swdlib.h"
#include "klflash.h"

#include "board.h"
#include "misc.h"
#include "gpio.h"
#include "led.h"
#include "ep0.h"
#include "target.h"


static struct swd swd;


/* ----- Target interface -------------------------------------------------- */


static void kl_pulse(void)
{
	gpio_set(TGT_SWD_CLK);
	gpio_clr(TGT_SWD_CLK);
}


static void tgt_send(void *ctx, uint32_t data, uint8_t bits)
{
	bool *input = ctx;
        uint8_t i;

	if (*input) {
		kl_pulse();
		gpio_output(TGT_SWD_DIO);
		*input = 0;
	}
	for (i = 0; i != bits; i++) {
		if ((data >> i) & 1)
			gpio_set(TGT_SWD_DIO);
		else
			gpio_clr(TGT_SWD_DIO);
		kl_pulse();
	}
}


static uint32_t tgt_recv(void *ctx, uint8_t bits)
{
	bool *input = ctx;
	uint32_t val = 0;
	uint8_t i;

	if (!*input) {
		gpio_input(TGT_SWD_DIO);
		kl_pulse();
		*input = 1;
	}
	for (i = 0; i != bits; i++) {
		val |= gpio_read(TGT_SWD_DIO) << i;
		kl_pulse();
	}
	return val;
}


static void tgt_reset(void *ctx, bool active)
{
	if (active) {
		gpio_clr(TGT_nRESET);
		gpio_output(TGT_nRESET);
		udelay(1);
	} else {
		gpio_input(TGT_nRESET);
		udelay(10);
	}
}


static void sleep_1s(void *ctx)
{
	mdelay(1000);
}


static struct swd_ops tgt_ops = {
	.send		= tgt_send,
	.recv		= tgt_recv,
	.reset		= tgt_reset,
	.sleep_1s	= sleep_1s,
	.report		= report,
};


/* ----- Target state ------------------------------------------------------ */


static bool is_open = 0;


static void tgt_close(void)
{
	swd_close(&swd);
}


static bool tgt_open(void)
{
	static bool input;

	if (is_open)
		swd_close(&swd);
	tgt_init();
	input = 0;
	return swd_open(&swd, &tgt_ops, &input);
}


/* ----- USB operations ---------------------------------------------------- */


static bool released = 0;


bool tgt_attach(void)
{
	released = 0;
	return tgt_open();
}


bool tgt_detach(void)
{
	tgt_close();
	return 1;
}


bool tgt_acquire(void)
{
	if (!swd_release(&swd))
		return 0;
	released = 1;
	return 1;
}


bool tgt_identify(void)
{
	bool can_erase, secure, backdoor;

	if (!kl_flash_security(&swd, &can_erase, &secure, &backdoor))
		return 0;
	if (!secure) {
		if (!released && !tgt_acquire())
			return 0;
		if (!swd_identify_soc(&swd))
			return 0;
	}
	report("Mass-erase: %s, secure: %s, backdoor: %s\n",
	    can_erase ? "yes" : "no", secure ? "yes" : "no",
	    backdoor ? "yes" : "no");
	return 1;
}


bool tgt_erase(void)
{
	bool ok;

	led(1);
	ok = kl_mass_erase(&swd);
	led(0);
	return ok;
}


bool tgt_write(const uint32_t *data, uint32_t offset, uint16_t words)
{
	bool ok;

	/* @@@ enter correct state, check security */
	led(1);
	ok = kl_program_flash(&swd, offset, data, words, NULL, NULL);
	led(0);
	return ok;
}


bool tgt_read(uint32_t *buf, uint32_t offset, uint16_t words)
{
	led(1);
	while (words--) {
		if (!swd_read_32(&swd, (uint32_t *) offset, buf++)) {
			led(0);
			return 0;
		}
		offset += 4;
	}
	led(0);
	return 1;
}


/* ----- Initialization ---------------------------------------------------- */


void tgt_init(void)
{
	gpio_init_in(TGT_nRESET, 1);
	gpio_init_out(TGT_SWD_DIO, 1);
	gpio_init_out(TGT_SWD_CLK, 1);
	gpio_begin(TGT_nRESET);
	gpio_begin(TGT_SWD_DIO);
	gpio_begin(TGT_SWD_CLK);
}
