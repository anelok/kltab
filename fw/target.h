/*
 * fw/target.h - SWD target control
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef TARGET_H
#define	TARGET_H

#include <stdbool.h>
#include <stdint.h>


bool tgt_attach(void);
bool tgt_detach(void);
bool tgt_acquire(void);
bool tgt_identify(void);
bool tgt_erase(void);
bool tgt_write(const uint32_t *data, uint32_t offset, uint16_t words);
bool tgt_read(uint32_t *buf, uint32_t offset, uint16_t words);

void tgt_init(void);

#endif /* !TARGET_H */
