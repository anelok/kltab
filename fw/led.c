/*
 * fw/led.c - LED control
 *
 * Written 2013-2014 by Werner Almesberger
 * Copyright 2013-2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>

#include "board.h"
#include "gpio.h"
#include "led.h"


void led(bool on)
{
	if (on)
		led_on(LED);
	else
		led_off(LED);
}


void led_on(gpio_id id)
{
	gpio_begin(id);
	if (id == LED_A || id == LED_B)
		gpio_set(id);
	else
		gpio_clr(id);
	gpio_end(id);
}


void led_off(gpio_id id)
{
	gpio_begin(id);
	if (id == LED_A || id == LED_B)
		gpio_clr(id);
	else
		gpio_set(id);
	gpio_end(id);
}


void led_init(void)
{
	gpio_init_out(LED_A, 0);
	gpio_init_out(LED_B, 0);
	gpio_init_out(nLED_C, 1);
	gpio_init_out(nLED_D, 1);
}
