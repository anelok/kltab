/*
 * tool.c - KLtab control tool
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <usb.h>

#include "usbopen.h"

#include "../fw/ep0.h"


#define	USB_VENDOR	0x20b7
#define	USB_PRODUCT	0xb170

#define	TIMEOUT_MS	2000


static usb_dev_handle *dev;
static bool debug = 0;


/* ----- USB operations ---------------------------------------------------- */


static bool query(void)
{
	char buf[255];
	int res;

	if (debug)
		fprintf(stderr, "%s <---\n", "PGM_QUERY");
	res = usb_control_msg(dev, PGM_QUERY & 0xff, PGM_QUERY >> 8,
            0, 0, buf, sizeof(buf), TIMEOUT_MS);
	if (res <= 0) {
		fprintf(stderr, "PGM_QUERY: %d\n", res);
		exit(1);
	}
	fprintf(stderr, "%s%.*s", *buf ? "Error: " : "", res-1, buf+1);
	return !*buf;
}


static void check_outcome(int res, const char *req_name)
{
	if (res < 0) {
		fprintf(stderr, "%s: %d\n", req_name, res);
		exit(1);
	}
	if (!query())
		exit(1);
}


static void do_usb_send(uint16_t req, const char *req_name,
   uint16_t value, uint16_t idx, const void *buf, uint16_t size)
{
	int res;

	if (debug)
		fprintf(stderr, "%s --->\n", req_name);
	res = usb_control_msg(dev, req & 0xff, req >> 8,
	    value, idx, (void *) buf, size, TIMEOUT_MS);
	check_outcome(res, req_name);
}


static int do_usb_recv(uint16_t req, const char *req_name,
   uint16_t value, uint16_t idx, void *buf, uint16_t size)
{
	int res;

	if (debug)
		fprintf(stderr, "%s <---\n", req_name);
	res = usb_control_msg(dev, req & 0xff, req >> 8,
	    value, idx, buf, size, TIMEOUT_MS);
	check_outcome(res, req_name);
	return res;
}


#define	usb_send(req, value, idx, buf, size) \
	do_usb_send(req, #req, value, idx, buf, size)
#define	usb_recv(req, value, idx, buf, size) \
	do_usb_recv(req, #req, value, idx, buf, size)


/* ----- Operations -------------------------------------------------------- */


static void identify(void)
{
	usb_send(PGM_IDENTIFY, 0, 0, NULL, 0);
}


static void erase(void)
{
	usb_send(PGM_ERASE, 0, 0, NULL, 0);
}


static void flash(FILE *file, const char *name)
{
	uint8_t buf[128];
	uint32_t addr = 0;
	ssize_t size;

	usb_send(PGM_ERASE, 0, 0, NULL, 0);
	while (1) {
		size = fread(buf, 1, sizeof(buf), file);
		if (!size)
			break;
		if (size < 0) {
			perror(name);
			exit(1);
		}
		if (size & 3) {
			memset(buf+size, 0, 4 - (size & 3));
			size = (size + 3) & ~3;
		}
		addr += size;
		usb_send(PGM_WRITE, addr, addr >> 16, buf, size);
	}
}


static void dump(void)
{
	uint8_t buf[128];
	uint32_t addr;
	ssize_t size;

	for (addr = 0; addr != 128*1024; addr += sizeof(buf)) {
		size = usb_recv(PGM_READ, addr, addr >> 16, buf, sizeof(buf));
		if (size != sizeof(buf)) {
			fprintf(stderr, "PGM_READ: short read %d < %u\n",
			    (int) size, (unsigned) sizeof(buf));
			exit(1);
		}
		size = fwrite(buf, 1, sizeof(buf), stdout);
		if (size < 0) {
			perror(NULL);
			exit(1);
		}
		if (size != sizeof(buf)) {
			fprintf(stderr, "short write: %d < %u\n",
			    (int) size, (unsigned) sizeof(buf));
			exit(1);
		}
	}
	if (fflush(stdout)) {
		perror(NULL);
		exit(1);
	}
}


/* ----- Main -------------------------------------------------------------- */


static void usage(const char *name)
{
	fprintf(stderr,
"usage: %s [-D] [file.bin]\n"
"       %s [-D] -d\n"
"       %s [-D] -e\n\n"
"  -d  dump Flash content (in binary)\n"
"  -D  enable debugging output\n"
"  -e  erase whole Flash\n"
	    , name, name, name);
	exit(1);
}


int main(int argc, char **argv)
{
	enum {
		mode_default = 0,
		mode_dump,
		mode_erase,
        } mode = mode_default;
	FILE *file = NULL;
	int c;

	while ((c = getopt(argc, argv, "dDe")) != EOF)
		switch (c) {
		case 'd':
			if (mode)
				usage(*argv);
			mode = mode_dump;
			break;
		case 'D':
			debug = 1;
			break;
		case 'e':
			if (mode)
				usage(*argv);
			mode = mode_erase;
			break;
                default:
                        usage(*argv);
                }

	switch (argc-optind) {
	case 1:
		if (mode != mode_default)
			usage(*argv);
		file = fopen(argv[optind], "r");
		if (!file) {
			perror(argv[optind]);
			return 1;
		}
		break;
	case 0:
		break;
	default:
		usage(*argv);
        }

	usb_unrestrict();
	dev = open_usb(USB_VENDOR, USB_PRODUCT);
	if (!dev) {
		fprintf(stderr, ":-(\n");
		return 1;
	}

	usb_send(PGM_ATTACH, 0, 0, NULL, 0);
	usb_send(PGM_ACQUIRE, 0, 0, NULL, 0);

	switch (mode) {
	case mode_default:
		if (file)
			flash(file, argv[optind]);
		else
			identify();
		break;
	case mode_erase:
		erase();
		break;
	case mode_dump:
		dump();
		break;
	default:
		abort();
	}

	usb_send(PGM_DETACH, 0, 0, NULL, 0);

	return 0;
}
